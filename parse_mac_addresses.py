import os
import textfsm


def count_unique_mac_address(mac_address_table):
    unique_mac_address = list()

    for mac_address in mac_address_table:
        if mac_address[0] not in unique_mac_address and mac_address[1] == 'DYNAMIC':
            unique_mac_address.append(mac_address[0])

    print('{} unique MAC addresses found\n'.format(len(unique_mac_address)))


def count_and_print_unique_ports(mac_address_table):
    unique_ports = list()

    for mac_address in mac_address_table:
        if mac_address[3] not in unique_ports and mac_address[1] == 'DYNAMIC':
            unique_ports.append(mac_address[3])

    print('{} unique ports found'.format(len(unique_ports)))
    print('Port list: {}\n'.format(sorted(unique_ports)))


def count_mac_address_per_vlan(mac_address_table):
    vlan_counters = dict()

    for mac_address in mac_address_table:
        # Only Dynamic MAC address
        if mac_address[1] == 'DYNAMIC':
            # Is there already a counter for a VLAN? If not create one, with the value of 1
            if mac_address[2] not in vlan_counters:
                vlan_counters[mac_address[2]] = 1
            # If the counter for the VLAN already exists, add 1 to the counter
            elif mac_address[2] in vlan_counters:
                vlan_counters[mac_address[2]
                              ] = vlan_counters[mac_address[2]] + 1

    print('MAC address per VLAN found:')
    for vlan in sorted(vlan_counters):
        print('VLAN {}: {} MAC address'.format(vlan, vlan_counters[vlan]))


if __name__ == '__main__':
    templates_dir = 'templates'

    with open('mac-address-table.log', 'r') as f:
        mac_address_table_output = f.read()

    with open(os.path.join(templates_dir, 'cisco_ios_show_mac-address-table.textfsm'), 'r') as f:
        template = textfsm.TextFSM(f)

    # Parse the 'show interfaces' template against the configuration file
    mac_address_table = template.ParseText(mac_address_table_output)

    # Count the unique mac address in the table
    count_unique_mac_address(mac_address_table)

    # Count and print unique ports
    count_and_print_unique_ports(mac_address_table)

    # Count MAC address per VLAN
    count_mac_address_per_vlan(mac_address_table)
